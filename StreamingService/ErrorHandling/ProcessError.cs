﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreamingService
{
    public class ProcessError
    {
        #region Send Error Email
        public void SendErrorEmail(string Method, string Error, string StackTrace)
        {
            try
            {
                string body = string.Empty;
                body = "<html>EDBS has reported an error. The details of the exception are below:<br /><br /><table border=\"0\" cellspacing=\"2\" cellpadding=\"2\"><tr><td width=\"100\"><b>Time:</b></td><td>" + DateTime.Now;
                body += "</td></tr><tr><td><b>User:</b></td><td>" + HttpContext.Current.User.Identity.Name;
                body += "</td></tr><tr><td><b>Method:</b></td><td>" + Method;
                body += "</td></tr><tr><td valign=\"top\"><b>Exception Message:</b></td><td valign=\"top\">" + Error;
                body += "</td></tr><tr><td valign=\"top\"><b>Stack Trace:</b></td><td valign=\"top\">" + StackTrace;
                body += "</td></tr></table></html>";

                SendMail sm = new SendMail();
                string FromEmail = System.Configuration.ConfigurationManager.ConnectionStrings["ErrorReportFromEmail"].ToString();
                string ToEmail = System.Configuration.ConfigurationManager.ConnectionStrings["ErrorReportEmails"].ToString();
                sm.SendEMailMessage("Error Handler", FromEmail, ToEmail, null, null, "EDBS Error Report", body);
            }
            catch (Exception)
            {
            }
        }
        #endregion
    }
}