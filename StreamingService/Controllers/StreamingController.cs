﻿using Newtonsoft.Json;
using StreamingService.Controllers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Filters;
using Sa;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data;
using StreamingService.Filter;

namespace StreamingService
{
    [HMACAuthentication]
    public class StreamingController : ApiController
    {
        DataAccess da = new DataAccess();

        public StreamingController()
        {
        }

        public StreamingController(HttpRequestMessage clientRequest)
        {
            Request = clientRequest;
        }

        #region Get Property Keys For Dynamic
        public Dictionary<string, object> GetPropertyKeysForDynamic(dynamic dynamicToGetPropertiesFor)
        {
            Newtonsoft.Json.Linq.JObject attributesAsJObject = dynamicToGetPropertiesFor;
            Dictionary<string, object> values = attributesAsJObject.ToObject<Dictionary<string, object>>();
            return values;
        }
        #endregion

        #region Deserialize JSON
        public dynamic deserializeJSON(Task<string> Content)
        {
            try
            {
                dynamic jsonObject = JsonConvert.DeserializeObject(Content.Result);
                return jsonObject;
            }
            catch (Exception ex)
            {
                return createErrorResponse(HttpStatusCode.BadRequest, "Invalid JSON detected.", "Invalid JSON detected.");
            }
        }
        #endregion

        #region Serialize JSON
        public string serializeJSON(DataSet ds)
        {
			string jsonObject = string.Empty;
			try
			{
				jsonObject = JsonConvert.SerializeObject(ds);
			}
			catch (Exception ex)
			{
				throw ex;
				//createErrorResponse(HttpStatusCode.BadRequest, "Invalid JSON detected.", "Invalid JSON detected.");
			}
			return jsonObject;

        }
        #endregion

        #region Create Error Response
        public HttpResponseMessage createErrorResponse(HttpStatusCode Code, string ReasonPhrase, string Content)
        {
            var ErrResponse = new HttpResponseMessage(Code);
            ErrResponse.ReasonPhrase = ReasonPhrase;
            ErrResponse.Content = new StringContent(Content);
            return ErrResponse;
        }
        #endregion

        #region EDBS Response
        public HttpResponseMessage EDBSResponse(string Procedure)
        {
            dynamic jsonObject = deserializeJSON(Request.Content.ReadAsStringAsync());

            Dictionary<string, object> JSONDic = GetPropertyKeysForDynamic(jsonObject);

            Dictionary<string, object> valueList = new Dictionary<string, object>();
            foreach (string key in JSONDic.Keys)
            {
                valueList.Add(key.ToLower(),JSONDic[key]);
            }

            List<string> propertyList = new List<string>();
            foreach (string key in JSONDic.Keys)
            {
                propertyList.Add(key.ToLower());
            }

            if (!propertyList.Contains("appid"))
            {
                return createErrorResponse(HttpStatusCode.BadRequest, "Element APPId Missing.", "Element APPId Missing.");
            }

            if (!propertyList.Contains("startdate"))
            {
                return createErrorResponse(HttpStatusCode.BadRequest, "Element StartDate Missing.", "Element StartDate Missing.");
            }
            
            DateTime StartDate;
            if (!DateTime.TryParse(valueList["startdate"].ToString(), out StartDate))
            {
                return createErrorResponse(HttpStatusCode.BadRequest, "Invalid StartDate. Expected date format is YYYY/MM/DD.", "Invalid StartDate. Expected date format is YYYY/MM/DD.");
            }

            string APPId = valueList["appid"].ToString();

			DataSet ds;
			try
			{
				ds = da.getData(Procedure, da.getCompanyID(APPId), StartDate);
			}
			catch (Exception ex)
			{
				ProcessError pe = new ProcessError();
				pe.SendErrorEmail("EDBSResponse getData", ex.Message, ex.StackTrace);
				return createErrorResponse(HttpStatusCode.InternalServerError, "Internal server error. Please contact Solomon for assistance.", "Internal server error. Please contact Solomon for assistance.");
			}
			string str;
			
			try
			{
				str = serializeJSON(ds);
			}
			catch (Exception ex)
			{
				ProcessError pe = new ProcessError();
				pe.SendErrorEmail("EDBSResponse serializeJSON", ex.Message, ex.StackTrace);
				return createErrorResponse(HttpStatusCode.InternalServerError, "Internal server error. Please contact Solomon for assistance.", "Internal server error. Please contact Solomon for assistance.");
			}
			
			byte[] buffer;
			try
			{
				buffer = System.Text.UTF8Encoding.UTF8.GetBytes(str);
			}
			catch (Exception ex)
			{
				ProcessError pe = new ProcessError();
				pe.SendErrorEmail("EDBSResponse GetBytes", ex.Message, ex.StackTrace);
				return createErrorResponse(HttpStatusCode.InternalServerError, "Internal server error. Please contact Solomon for assistance.", "Internal server error. Please contact Solomon for assistance.");
			}

			byte[] encryptedBuffer;
			try
			{
				encryptedBuffer = Sa.Crypto.Encrypt(buffer, System.Text.Encoding.UTF8.GetBytes(APPId), System.Text.Encoding.UTF8.GetBytes(da.getAPIKey(APPId)));
			}
			catch (Exception ex)
			{
				ProcessError pe = new ProcessError();
				pe.SendErrorEmail("EDBSResponse Encrypt", ex.Message, ex.StackTrace);
				return createErrorResponse(HttpStatusCode.InternalServerError, "Internal server error. Please contact Solomon for assistance.", "Internal server error. Please contact Solomon for assistance.");
			}

			HttpResponseMessage response = Request.CreateResponse();
			response.Content = new PushStreamContent(async (outputSteam, httpContent, transportContext) =>
            {
               
                await outputSteam.WriteAsync(encryptedBuffer, 0, encryptedBuffer.Length);

                outputSteam.Close();
            });
			
            return response;
        }
        #endregion

        [HttpPost]
        [ActionName("getProcessData")]
        public HttpResponseMessage GetProcessData()
        {
            return EDBSResponse("web.ProcessData");
        }

        [HttpPost]
        [ActionName("getCTMaterialReports")]
        public HttpResponseMessage GetCTMaterialReports()
        {
            return EDBSResponse("web.CTMaterialReports");
        }

        [HttpPost]
        [ActionName("getCTPricesByBlend")]
        public HttpResponseMessage CTPricesByBlend()
        {
            return EDBSResponse("web.CTPricesByBlend");
        }

        [HttpPost]
        [ActionName("getFurnacePeerGroupDef")]
        public HttpResponseMessage FurnacePeerGroupDef()
        {
            return EDBSResponse("web.FurnacePeerGroupDef");
        }

        [HttpPost]
        [ActionName("getLubePAProductSlateDetails")]
        public HttpResponseMessage LubePAProductSlateDetails()
        {
            return EDBSResponse("web.LubePAProductSlateDetails");
        }

        [HttpPost]
        [ActionName("getLubePAReturnsAndFuelProductSales")]
        public HttpResponseMessage LubePAReturnsAndFuelProductSales()
        {
            return EDBSResponse("web.LubePAReturnsAndFuelProductSales");
        }

        [HttpPost]
        [ActionName("getOutputFiles")]
        public HttpResponseMessage OutputFiles()
        {
            return EDBSResponse("web.OutputFiles");
        }

        [HttpPost]
        [ActionName("getOutputFileSheetColumnHeaders")]
        public HttpResponseMessage OutputFileSheetColumnHeaders()
        {
            return EDBSResponse("web.OutputFileSheetColumnHeaders");
        }

        [HttpPost]
        [ActionName("getOutputFileSheetColumns")]
        public HttpResponseMessage OutputFileSheetColumns()
        {
            return EDBSResponse("web.OutputFileSheetColumns");
        }

        [HttpPost]
        [ActionName("getOutputFileSheetRows")]
        public HttpResponseMessage OutputFileSheetRows()
        {
            return EDBSResponse("web.OutputFileSheetRows");
        }

        [HttpPost]
        [ActionName("getOutputFileSheets")]
        public HttpResponseMessage OutputFileSheets()
        {
            return EDBSResponse("web.OutputFileSheets");
        }

        [HttpPost]
        [ActionName("getPeerGroupDef")]
        public HttpResponseMessage PeerGroupDef()
        {
            return EDBSResponse("web.PeerGroupDef");
        }

        [HttpPost]
        [ActionName("getProcessPeers")]
        public HttpResponseMessage ProcessPeers()
        {
            return EDBSResponse("web.ProcessPeers");
        }

        [HttpPost]
        [ActionName("getProcessVsPeer")]
        public HttpResponseMessage ProcessVsPeer()
        {
            return EDBSResponse("web.ProcessVsPeer");
        }

        [HttpPost]
        [ActionName("getQuartiles")]
        public HttpResponseMessage Quartiles()
        {
            return EDBSResponse("web.Quartiles");
        }

        [HttpPost]
        [ActionName("getRanking")]
        public HttpResponseMessage Ranking()
        {
            return EDBSResponse("web.Ranking");
        }

        [HttpPost]
        [ActionName("getRefineryData")]
        public HttpResponseMessage RefineryData()
        {
            return EDBSResponse("web.RefineryData");
        }

        [HttpPost]
        [ActionName("getRefineryPeers")]
        public HttpResponseMessage RefineryPeers()
        {
            return EDBSResponse("web.RefineryPeers");
        }

        [HttpPost]
        [ActionName("getRefineryVsPeer")]
        public HttpResponseMessage RefineryVsPeer()
        {
            return EDBSResponse("web.RefineryVsPeer");
        }

        [HttpPost]
        [ActionName("getSections")]
        public HttpResponseMessage Sections()
        {
            return EDBSResponse("web.Sections");
        }

        [HttpPost]
        [ActionName("getVarDef")]
        public HttpResponseMessage VarDef()
        {
            return EDBSResponse("web.VarDef");
        }

		[HttpPost]
		[ActionName("getUnitsOfMeasure")]
		public HttpResponseMessage UnitsOfMeasure()
		{
			return EDBSResponse("web.UnitsOfMeasure");
		}

		[HttpPost]
		[ActionName("getVariableLinkUSMetric")]
		public HttpResponseMessage VariableLinkUSMetric()
		{
			return EDBSResponse("web.VariableLinkUSMetric");
		}

		[HttpPost]
		[ActionName("getVarHierarchyList")]
		public HttpResponseMessage VarHierarchyList()
		{
			return EDBSResponse("web.VarHierarchyList");
		}

		[HttpPost]
		[ActionName("getVarHierarchyItems")]
		public HttpResponseMessage VarHierarchyItems()
		{
			return EDBSResponse("web.VarHierarchyItems");
		}

		[HttpPost]
		[ActionName("getPeerGroupHierarchyGroupTypes")]
		public HttpResponseMessage PeerGroupHierarchyGroupTypes()
		{
			return EDBSResponse("web.PeerGroupHierarchyGroupTypes");
		}

		[HttpPost]
		[ActionName("getPeerGroupHierarchyMajorGroups")]
		public HttpResponseMessage PeerGroupHierarchyMajorGroups()
		{
			return EDBSResponse("web.PeerGroupHierarchyMajorGroups");
		}

		[HttpPost]
		[ActionName("getPeerGroupHierarchyList")]
		public HttpResponseMessage PeerGroupHierarchyList()
		{
			return EDBSResponse("web.PeerGroupHierarchyList");
		}

		[HttpPost]
		[ActionName("getPeerGroupHierarchyItems")]
		public HttpResponseMessage PeerGroupHierarchyItems()
		{
			return EDBSResponse("web.PeerGroupHierarchyItems");
		}

		[HttpPost]
		[ActionName("getRegions")]
		public HttpResponseMessage Regions()
		{
			return EDBSResponse("web.Regions");
		}

		[HttpPost]
		[ActionName("getStudies")]
		public HttpResponseMessage Studies()
		{
			return EDBSResponse("web.Studies");
		}




        
    }
}
