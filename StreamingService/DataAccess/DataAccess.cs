﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net;

namespace StreamingService
{
    public class DataAccess
    {
        private static string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ResultsDBConnectionStringSQLClient"].ToString();

        #region Get DataSet
        private DataSet getDataSet(string CommandText, string CompanyID, DateTime DateRange)
        {
             using (SqlConnection conn = new SqlConnection(ConnectionString))
             {
                 using (SqlCommand cmd = new SqlCommand(CommandText, conn))
                 {
                     conn.Open();
                     cmd.CommandType = CommandType.StoredProcedure;
                     cmd.Parameters.Add(new SqlParameter("CompanyID", CompanyID));
                     cmd.Parameters.Add(new SqlParameter("DataPullStartDate", DateRange));
                     SqlDataAdapter sda = new SqlDataAdapter(cmd);
                     DataSet ds = new DataSet();
                     sda.Fill(ds);
                     return ds;
                 }
             }
        }
        #endregion

        #region Is Valid APPId is valid
        public bool isValidAPPId(string APPId)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("ValidateAPPId", conn))
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@APPId", APPId));
                        if (cmd.ExecuteScalar() != null)
                            return true;
                        else
                            return false;
                    }
                }
            }
            catch (Exception ex)
            {
                ProcessError pe = new ProcessError();
                pe.SendErrorEmail("isValidAPPId", ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion

        #region Get API Key
        public string getAPIKey(string APPId)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("GetAPIKey", conn))
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@APPId", APPId));
                        return cmd.ExecuteScalar().ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ProcessError pe = new ProcessError();
                pe.SendErrorEmail("getAPIKey", ex.Message, ex.StackTrace);
                return string.Empty;
            }
        }
        #endregion

        #region Get Company ID
        public string getCompanyID(string APPId)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("GetCompanyID", conn))
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@APPId", APPId));
                        return cmd.ExecuteScalar().ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ProcessError pe = new ProcessError();
                pe.SendErrorEmail("getCompanyID", ex.Message, ex.StackTrace);
                return string.Empty;
            }
        }
        #endregion

        #region Get Data
        public DataSet getData(string Procedure, string CompanyID, DateTime DateRange)
        {
            DataSet ds = new DataSet();
            try
            {
                ds = getDataSet(Procedure, CompanyID, DateRange);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        #endregion

        




    }
}