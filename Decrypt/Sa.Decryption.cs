﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Web;
using System.Text;


namespace Sa
{
	/// <summary>
	/// Performs the encryption, decryption, and verification of a byte array.
	/// The byte array is signed during the encryption process. During decryption, the signature is verified.
	/// </summary>
	public static class Decryption
	{

		public static byte[] Decrypt(byte[] signedCryptoText, string password, string signatureKey)
		{
			byte[] passwordBytes = Encoding.ASCII.GetBytes(password);
			byte[] signatureKeyBytes = Encoding.ASCII.GetBytes(signatureKey);

			return Decrypt(signedCryptoText, passwordBytes, signatureKeyBytes);

		}

		/// <summary>
		/// Verifies, decrypts, then expands a cypher text byte array
		/// </summary>
		/// <param name="signedCryptoText">Cypher text byte array to be decrypted and expanded</param>
		/// <param name="password">Encryption Key</param>
		/// <param name="signatureKey">Signing Key</param>
		/// <param name="salt">Initilization vector (at least 8 bytes)</param>
		/// <returns>Validated, decrypted and expanded byte array</returns>
		public static byte[] Decrypt(byte[] signedCryptoText, byte[] password, byte[] signatureKey)
		{
			//creates its own salt by concatenating the password and signature
			var s = new MemoryStream();
			s.Write(password, 0, password.Length);
			s.Write(signatureKey, 0, signatureKey.Length);
			byte[] salt = s.ToArray();

			byte[] signature;
			byte[] cryptoText;

			Bytes.Split(signedCryptoText, Signature.Length, out signature, out cryptoText);

			if (Signature.Verify(cryptoText, signatureKey, signedCryptoText))
			{
				return Sa.Decryption.Decrypt(cryptoText, password, salt, true);
			}
			else
			{
				return new byte[0];
			}
		}


		/// <summary>
		/// Decrypts then expands a cypher text byte array
		/// </summary>
		/// <param name="cryptoText">Cypher text byte array to be decrypted and expanded</param>
		/// <param name="password">Encryption key</param>
		/// <param name="salt">Initilization vector (at least 8 bytes)</param>
		/// <returns>Decrypted and expanded byte array</returns>
		private static byte[] Decrypt(byte[] cryptoText, byte[] password, byte[] salt, bool isInternalFunction)
		{
			//isInternalFunction is only used to change the method signature, so we could add a Decrypt function that only accepts a password and signature

			//	Decrypt Bytes
			byte[] PlainText;
			AesCryptography.DecryptBytes(cryptoText, password, salt, out PlainText);

			//	Expand Bytes
			byte[] GZipExpanded;
			Zip.Expand(PlainText, out GZipExpanded);

			return GZipExpanded;
		}
	}

	static class AesCryptography
	{
		private const int AesBlockSize = 128;
		private const int AesFeedbackSize = 128;
		private const int AesKeySize = 256;
		private const int RfcIterations = 3001;

		private const CipherMode AesCipherMode = CipherMode.CBC;
		private const PaddingMode AesPaddingMode = PaddingMode.ANSIX923;

		internal static void EncryptBytes(byte[] plainText, byte[] password, byte[] salt, out byte[] cryptoText)
		{
			if (VerifySaltLength(salt))
			{
				using (AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
				{
					aes.BlockSize = AesBlockSize;
					aes.FeedbackSize = AesFeedbackSize;
					aes.KeySize = AesKeySize;
					aes.Mode = AesCipherMode;
					aes.Padding = AesPaddingMode;

					using (Rfc2898DeriveBytes rfcKey = new Rfc2898DeriveBytes(password, salt, RfcIterations))
					{
						using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
						{
							rng.GetBytes(aes.IV);
						}

						aes.Key = rfcKey.GetBytes(aes.KeySize / 8);

						using (MemoryStream ms = new MemoryStream())
						{
							using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
							{
								cs.Write(plainText, 0, plainText.Length);
								cs.Close();
							}
							cryptoText = aes.IV.Concat(ms.ToArray()).ToArray();
							ms.Close();
						}
					}
					aes.Clear();
				}
			}
			else
			{
				cryptoText = new byte[0];
			}
		}

		internal static void DecryptBytes(byte[] cryptoText, byte[] password, byte[] salt, out byte[] plainText)
		{
			try
			{
				if (VerifySaltLength(salt))
				{
					using (AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
					{
						aes.BlockSize = AesBlockSize;
						aes.FeedbackSize = AesFeedbackSize;
						aes.KeySize = AesKeySize;
						aes.Mode = AesCipherMode;
						aes.Padding = AesPaddingMode;

						using (Rfc2898DeriveBytes rfcKey = new Rfc2898DeriveBytes(password, salt, RfcIterations))
						{
							byte[] iv;

							Bytes.Split(cryptoText, aes.IV.Length, out iv, out cryptoText);

							aes.IV = iv;

							aes.Key = rfcKey.GetBytes(aes.KeySize / 8);
							
							using (MemoryStream ms = new MemoryStream())
							{
								using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
								{
									cs.Write(cryptoText, 0, cryptoText.Length);
									cs.Close();
								}
								plainText = ms.ToArray();
								ms.Close();
							}
						}
						aes.Clear();
					}
				}
				else
				{
					plainText = new byte[0];
				}
			}
			catch
			{
				plainText = new byte[0];
			}
		}

		private static bool VerifySaltLength(byte[] salt)
		{
			return (salt.Length >= 8);
		}
	}

	static class Signature
	{
		/// <summary>
		/// Length of the signature defined by the HMAC in Append()
		/// </summary>
		internal const int Length = 64;

		/// <summary>
		/// Appends the signature to the unsigned data.
		/// </summary>
		/// <param name="signatureKey">Signature key</param>
		/// <param name="unsignedRaw">Data to be signed</param>
		/// <returns></returns>
		internal static byte[] Append(byte[] signatureKey, byte[] unsignedRaw)
		{
			byte[] signature;

			using (HMACSHA512 hmac = new HMACSHA512(signatureKey))
			{
				signature = hmac.ComputeHash(unsignedRaw);
				hmac.Clear();
			}

			return signature.Concat(unsignedRaw).ToArray();
		}

		/// <summary>
		/// Verifies the raw data signed by the signature key matches the signed raw data.
		/// </summary>
		/// <param name="unsignedRaw"></param>
		/// <param name="signatureKey">Signature key</param>
		/// <param name="signedRaw">Signed data</param>
		/// <returns></returns>
		internal static bool Verify(byte[] unsignedRaw, byte[] signatureKey, byte[] signedRaw)
		{
			byte[] sig = Signature.Append(signatureKey, unsignedRaw);

			return sig.SequenceEqual(signedRaw);
		}
	}

	static class Bytes
	{
		internal static void Split(byte[] raw, int length, out byte[] primary, out byte[] remainder)
		{
			primary = raw.Take(length).ToArray();
			remainder = raw.Skip(length).Take(raw.Length - length).ToArray();
		}
	}
}